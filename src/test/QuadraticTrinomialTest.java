import org.junit.Assert;
import org.testng.annotations.*;
import org.testng.asserts.*;


public class QuadraticTrinomialTest {

    @DataProvider
    public static Object[][] polynomialsData() {
        return new Object[][]{
                {1,2,1, new double[]{-1, -1}},
                {1,0,0, new double[]{0, 0}},
                {1,4,3, new double[]{-1, -3}},
                {2,8,6, new double[]{-1, -3}},
                {1,0,1, new double[]{}}
        };
    }

    @DataProvider
    public static Object[][] badPolynomialsData() {
        return new Object[][]{
                {0,0,1},
                {0,1,1},
                {0,0,0}
        };
    }

    @Test(dataProvider = "polynomialsData")
    void solve(double a, double b, double c, double [] expected) {
        Assert.assertArrayEquals((new QuadraticTrinomial(a,b,c)).solve() , expected, 0.000001);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, dataProvider = "badPolynomialsData")
    public void testUnsolvable(double a, double b, double c) {
        QuadraticTrinomial fail = new QuadraticTrinomial(a,b,c);
    }

}