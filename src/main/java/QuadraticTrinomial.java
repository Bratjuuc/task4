import static java.lang.Math.*;

public class QuadraticTrinomial {
    public double a;
    public double b;
    public double c;

    public QuadraticTrinomial(double xa, double xb, double xc)throws IllegalArgumentException{
        if (Double.compare(xa, 0) == 0){
            throw new IllegalArgumentException();
        }
        a = xa;
        b = xb;
        c = xc;
    }

    public double [] solve (){
        double D = b*b -4*a*c;
        if (D < 0){
            return new double[0];
        }

        double d = sqrt(D);
        return new double[] {(-b + d) / (2 * a), (-b - d) / (2 * a)};
    }
}
